<?php  

	session_start();
  
    error_reporting(E_ERROR | E_PARSE);
	
    // Se o usuário após o login pressionar o botão de retorno do navegador, a sessão é destruída.
	//if (isset($_SESSION)) {	
	 //  session_destroy();
	//}
    //*******************************************************************************************

$email = $_GET['email'];
$senha = $_GET['senha'];

?>  


<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title>Paranhos - Administração de Delivery</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="images/favicon.png"/>
	<link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"> 
	<link rel="stylesheet" type="text/css" href="css/main.css">
    <script src="js/jquery-3.3.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>

<link href='https://fonts.googleapis.com/css?family=Francois+One' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Carrois+Gothic' rel='stylesheet' type='text/css'>
</head>
<body>

	<img src="./images/logo.png" class="paranhoslogo" alt="PARANHOS">
	<img src="./images/slide11-1.png" class="startnow" alt="PARANHOS">
	<!-- <img src="./images/img2delivery.png" class="bgh" alt="PARANHOS"> -->

	<div id="palavras-banner">
												<span class="palavra-solucoes"> ADMINISTRAÇÃO</span>
												<span class="palavra-simbolo">&amp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
												<span class="palavra-solucoes"> CONTROLE<span class="palavra-simbolo">&nbsp;&nbsp;&nbsp;</span></span><br>
												<span class="palavra-digital"> INSTANTÂNEO</span>


											</div>


	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">   <!-- p-t-55 p-b-20 -->
			
				<form class="login100-form validate-form" id="formLogin" name="formLogin" role="form" method="post" action="">
					<span class="login100-form-title">
						Administre seu Delivery
					</span>
					<span class="login100-form-avatar">
						<img src="images/avatar-01.jpg" alt="AVATAR">
					</span>

                   
					<div  class="wrap-input100 validate-input">
						<input class="input100" type="username" name="email" id="email" value="<?php echo $email; ?>" placeholder="e-mail">
					</div>

					<div class="wrap-input100 validate-input">
						<input class="input100" type="password" name="senha" id="senha" value="<?php echo $senha; ?>" placeholder="senha">
						<input class="form-control"  name="controle" id="controle" type="hidden" value="345067821133">
					</div>

                    <br>
					
					<div class="container-login100-form-btn">
						<button class="login100-form-btn" type="button" name="entrar" id="entrar">
							Login
						</button>
					</div>
				</form>
				
			</div>
		</div>
		

			<!-- Modal Alert -->
			<div class="modal fade in" id="myAlert" data-keyboard="false" data-backdrop="static">
			  <div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Atenção</h4>
				  </div>
				  <div class="modal-body">
					<p>Texto</p>
				  </div>
				  <div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
				  </div>
				</div>
			  
			  </div>
			</div>
			<!-- Fim Modal Alert -->
	
	</div>


		  

	<script src='js/index.js'></script>
	<!--<script src='js/index-min.js'></script>-->

</body>
</html>